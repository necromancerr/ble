package com.pratish.derive

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import java.sql.Connection

/**
 * Main class that displays UI to the user.
 */
class MainActivity : AppCompatActivity(), BluetoothManager.BluetoothManagerScanResult {
    private lateinit var bluetoothManager: BluetoothManager
    private lateinit var connectionManager: ConnectionManager

    private val delay:Long = 20000

    private val gattUpdateReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent!!.action
            when {
                ConnectionManager.CHARACTERISTICS_CHANGED == action -> {
                    // Get data from intent and display display to user
                }
                ConnectionManager.CHARACTERISTICS_READ == action -> {
                    // Get data from intent and display display to user
                }
                ConnectionManager.CHARACTERISTICS_WRITE == action -> {

                }
                ConnectionManager.DEVICE_CONNECTED == action -> {
                    // Notify the user that device has been connected.
                    // and that we are waiting for search services.
                }
                ConnectionManager.REGISTER_NOTIFICATION_FAILED == action -> {
                    // Notification registration failed.
                    // Rescan for device
                    bluetoothManager.startScan(delay)
                }
                ConnectionManager.SERVICE_DISCOVERY_FAILED == action -> {

                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bluetoothManager = BluetoothManager(this, "device UUID")
        bluetoothManager.startScan(delay)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun deviceFound(device: BluetoothDevice) {
        // Connect to the bluetooth device
        // device we are searching for has been found
        // Make a connection to the device.
        connectionManager = ConnectionManager(this, device)
        this.registerReceiver(gattUpdateReceiver, gattBroadcastFilter())
        connectionManager.connect()
    }

    override fun deviceNotFound(error: Error) {
        // Display message to user that the bluetooth device was not found.
        // prompt the user that network manager failed to find the device within 20 seconds.
        // Ask the user if they want to restart the search.
        // If they want to restart, call startScan again.
        bluetoothManager.startScan(delay)
    }

    private fun gattBroadcastFilter() : IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectionManager.CHARACTERISTICS_CHANGED)
        intentFilter.addAction(ConnectionManager.CHARACTERISTICS_READ)
        intentFilter.addAction(ConnectionManager.CHARACTERISTICS_WRITE)
        intentFilter.addAction(ConnectionManager.DEVICE_CONNECTED)
        intentFilter.addAction(ConnectionManager.REGISTER_NOTIFICATION_FAILED)
        intentFilter.addAction(ConnectionManager.SERVICE_DISCOVERY_FAILED)
        return intentFilter
    }


}
