package com.pratish.derive

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.os.Handler
import android.os.Parcel
import android.os.Parcelable
import java.io.IOException
import java.util.*

/**
 * BluetoothManager handles connecting to bluetooth device.
 * This can and needs to be modified with more information related to the bluetooth device
 * we are trying to connect to. Such as `name` and `UUID`
 *
 * @param c: Context to use to interface with bluetooth interface
 * @param address: Address of the bluetooth device.
 */
class BluetoothManager(c: Context, address: String) {

    interface BluetoothManagerScanResult {
        fun deviceFound(device: BluetoothDevice)
        fun deviceNotFound(error: Error)
    }

    var address: String
    private var context: Context

    private var bluetoothAdapter: BluetoothAdapter
    private var bluetoothLeScanner: BluetoothLeScanner
    private lateinit var scanFilters: MutableList<ScanFilter>
    private lateinit var scanSettings: ScanSettings
    private lateinit var scanCallback: ScanCallback
    lateinit var delegate: BluetoothManagerScanResult
    private lateinit var scanThread: Thread

    init {
        this.address = address
        this.context = c

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (bluetoothAdapter == null) {
            // Bluetooth is not available in this device. Show proper error
        }

        if (!bluetoothAdapter.isEnabled) {
            // Bluetooth is available but not enabled.
            // Prompt user to enable bluetooth using default android intents
        }

        bluetoothLeScanner = bluetoothAdapter.bluetoothLeScanner
        if (bluetoothLeScanner != null) {
            // Bluetooth found and is enabled. Start scanning

            // Only scan for device we are intereested in which is defined by the address.
            // This can be modified to include more filters based on our needs.
            val scanFilter: ScanFilter = ScanFilter.Builder().setDeviceAddress(this.address).build()

            // Set scan settings, High power, lower power, balannced.
            scanSettings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build()
            scanFilters = mutableListOf(scanFilter)


            // Set callback for the scanner
            scanCallback = object : ScanCallback() {
                override fun onScanResult(callbackType: Int, result: ScanResult?) {
                    super.onScanResult(callbackType, result)
                    val blueToothDevice = result!!.device
                    stopScan()
                    delegate?.deviceFound(blueToothDevice)
                }
            }
        }
    }

    /**
     * Start device scanner
     *
     * @param delay: Time interval to check how to wait before
     *               stoping search. Enter value in milliseconds
     *               Defaults to 20 seconds
     */
    fun startScan(delay: Long = 20000) {
        try {
            if (bluetoothLeScanner != null) {
                scanThread = Thread(Runnable {
                    bluetoothLeScanner.startScan(scanFilters, scanSettings, scanCallback)
                })
                scanThread.start()

                val handler = Handler()
                handler.postDelayed({
                    stopScan()
                    delegate?.deviceNotFound(error("Scan time out"))
                }, delay)

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Stop bluetooth scan
     */
    fun stopScan() {
        try {
            bluetoothLeScanner.stopScan(scanCallback)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}