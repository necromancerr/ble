package com.pratish.derive

import android.app.Service
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.os.Parcel
import android.os.Parcelable
import java.io.DataInput
import java.io.IOException
import java.lang.IllegalArgumentException

class ConnectionManager(c: Context, device: BluetoothDevice) : Service() {
    companion object {
        val DEVICE_CONNECTED = "deviceconnected"
        val SERVICE_DISCOVERY_FAILED = "service_discovery_failed"
        val CHARACTERISTICS_CHANGED = "characteristics_changed"
        val CHARACTERISTICS_READ = "characteristics_read"
        val CHARACTERISTICS_WRITE = "characteristics_write"
        val REGISTER_NOTIFICATION_FAILED = "register_notification_failed"
    }

    private var bluetoothAdapter: BluetoothAdapter
    private val binder: Binder? = null
    private val context : Context

    private var device: BluetoothDevice
    private lateinit var blueGatt: BluetoothGatt
    private var blueService: BluetoothGattService? = null
    private var blueCharacteristic: BluetoothGattCharacteristic? = null
    private var connectionThread: Thread? = null

    private var gattCallback: BluetoothGattCallback = object: BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                // Device has been connected
                // start service discovery
                broadcastUpdate(DEVICE_CONNECTED)
                gatt?.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                // Device disconnected. Handle gracefully
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            // Services has been discovered.
            if (status == BluetoothGatt.GATT_SUCCESS) {
                try {
                    // Service discovery was successful
                    for (service in gatt!!.services) {
                        // make sure we are dealing with service we are interested in
                        // Asuming we found the right service get the characteristics
                        blueService = service
                        for (characteristics in blueService!!.characteristics) {
                            // make sure we are dealing with the right characteristics
                            blueCharacteristic = characteristics

                            // If we find the right characteristics, register for notification
                            blueGatt.setCharacteristicNotification(blueCharacteristic, true)
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    broadcastUpdate(REGISTER_NOTIFICATION_FAILED)
                }
            } else {
                broadcastUpdate(SERVICE_DISCOVERY_FAILED)
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)

            broadcastUpdate(CHARACTERISTICS_CHANGED)
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                // We have successfully read data.
                // Get the data from characteristics and pass it to the activity waiting for it.
                // Pass the data using Intent.

                var data = characteristic!!.value

                broadcastUpdateWithData(CHARACTERISTICS_READ, data)
            }
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                // Pass more data here using Intent
                broadcastUpdate(CHARACTERISTICS_WRITE)
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder;
    }

    init {
        this.context = c
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        this.device = device

        //
        // Do same checks as in BluetoothManager class
        // To make sure bluetooth is still available, enabled and usable
        //
    }

    /**
     * Start bluetooth connection to the device
     *
     * @return Boolean: Returns true if connection was successful, false otherwise.
     */
    fun connect(){
        connectionThread = Thread( Runnable {
            try {
                // make sure bluetooth is available and enabled
                blueGatt = device.connectGatt(this.context, false, gattCallback)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        connectionThread!!.start()
    }

    /**
     * Disconnect device
     *
     * @return Boolean: Returns true if success, false otherwise
     */
    fun disconnect(): Boolean {
        try {
            if (blueGatt != null) {
                blueGatt.disconnect()
                blueGatt.close()
                blueService = null
                blueCharacteristic = null
                connectionThread!!.interrupt()
                connectionThread = null
                return true
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    /**
     * Send data to connected bluetooth device. This can be modified to handle
     * data with size greater than 20 by breaking the data down to 20 byte chunks
     *
     * @param data: Data to send to the bluetooth device.
     */
    fun sendDataToDevice(data: ByteArray) {
        if (data.size > 20) {
            throw IllegalArgumentException("datasize cannot be more than 20kb");
        }
        if (blueCharacteristic != null) {
            blueCharacteristic!!.value = data
        }
    }

    /**
     * Methods used to update the caller class that something has happened
     */
    private fun broadcastUpdate(action: String) {
        val intent = Intent(action)
        sendBroadcast(intent)
    }

    private fun broadcastUpdateWithData(action: String, data: ByteArray) {
        val intent = Intent(action)
        intent.putExtra("data", data)
        sendBroadcast(intent)
    }

}